package main

import (
	"context"
	"log"

	pb "service_s/genproto"

	"google.golang.org/grpc"
)

func main() {
	connection, err := grpc.Dial("localhost:8088", grpc.WithInsecure())
	if err != nil {
		log.Fatal("failed to connect server", err.Error())
	}
	defer connection.Close()

	client := pb.NewAddFunction_ServiceClient(connection)
	req := &pb.Request{
		X: 10,
		Y: 20,
	}
	reply, err := client.AddFunction(context.Background(), req)
	if err != nil {
		log.Fatal("failed to call addfunction", err)
	}
	log.Println("answer: ", reply.Answer)
}
